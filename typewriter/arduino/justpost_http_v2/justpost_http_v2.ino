
#include <Arduino.h>
#include <SoftwareSerial.h>
#include <WiFly.h>
#include "HTTPClient.h"


#define SSID      "SSID"
#define KEY       "PWD_KEY"
// WIFLY_AUTH_OPEN / WIFLY_AUTH_WPA1 / WIFLY_AUTH_WPA1_2 / WIFLY_AUTH_WPA2_PSK
#define AUTH      WIFLY_AUTH_WPA2_PSK

#define HTTP_POST_URL "http://typewriter.meen.ch/post"
#define HTTP_POST_DATA "arduino wrote:"

// Pins' connection
// Arduino       WiFly
//  2    <---->    TX
//  3    <---->    RX
SoftwareSerial uart(2, 3);
SoftwareSerial myserial(8, 9);

WiFly wifly(uart);
HTTPClient http;
char get;

void setup() {
  Serial.begin(9600);
  
  Serial.println("------- WIFLY HTTP --------");
  
  uart.begin(9600);         // WiFly UART Baud Rate: 9600
  Serial.println("wifly begin");

  delay(1000);
  wifly.reset();
  delay(1000);
  wifly.sendCommand("set comm remote 0\r");
  delay(100);

  // check if WiFly is associated with AP(SSID)
  if (!wifly.isAssociated(SSID)) {
    while (!wifly.join(SSID, KEY, AUTH)) {
      Serial.println("Failed to join " SSID);
      Serial.println("Wait 0.1 second and try again...");
      delay(100);
    }
    
    wifly.save();    // save configuration, 
  }
  Serial.println("ready to write");
}


void loop() {
int incomingByte; // for incoming serial data
char incoming_char;

    myserial.begin(9600);

    if (myserial.available() > 0) {
    incoming_char = myserial.read();

      Serial.println("");

      uart.begin(9600);
          
      switch (incoming_char){

      case ' ': while (http.post(HTTP_POST_URL, " ", 100) < 0) {};break;
      case 'a': while (http.post(HTTP_POST_URL, "a", 100) < 0) {};break;
      case 'b': while (http.post(HTTP_POST_URL, "b", 100) < 0) {};break;
      case 'c': while (http.post(HTTP_POST_URL, "c", 100) < 0) {};break;
      case 'd': while (http.post(HTTP_POST_URL, "d", 100) < 0) {};break;
      case 'e': while (http.post(HTTP_POST_URL, "e", 100) < 0) {};break;
      case 'f': while (http.post(HTTP_POST_URL, "f", 100) < 0) {};break;
      case 'g': while (http.post(HTTP_POST_URL, "g", 100) < 0) {};break;
      case 'h': while (http.post(HTTP_POST_URL, "h", 100) < 0) {};break;
      case 'i': while (http.post(HTTP_POST_URL, "i", 100) < 0) {};break;
      case 'j': while (http.post(HTTP_POST_URL, "j", 100) < 0) {};break;
      case 'k': while (http.post(HTTP_POST_URL, "k", 100) < 0) {};break;
      case 'l': while (http.post(HTTP_POST_URL, "l", 100) < 0) {};break;
      case 'm': while (http.post(HTTP_POST_URL, "m", 100) < 0) {};break;
      case 'n': while (http.post(HTTP_POST_URL, "n", 100) < 0) {};break;
      case 'o': while (http.post(HTTP_POST_URL, "o", 100) < 0) {};break;
      case 'p': while (http.post(HTTP_POST_URL, "p", 100) < 0) {};break;
      case 'q': while (http.post(HTTP_POST_URL, "q", 100) < 0) {};break;
      case 'r': while (http.post(HTTP_POST_URL, "r", 100) < 0) {};break;
      case 's': while (http.post(HTTP_POST_URL, "s", 100) < 0) {};break;
      case 't': while (http.post(HTTP_POST_URL, "t", 100) < 0) {};break;
      case 'u': while (http.post(HTTP_POST_URL, "u", 100) < 0) {};break;
      case 'v': while (http.post(HTTP_POST_URL, "v", 100) < 0) {};break;
      case 'w': while (http.post(HTTP_POST_URL, "w", 100) < 0) {};break;
      case 'x': while (http.post(HTTP_POST_URL, "x", 100) < 0) {};break;
      case 'y': while (http.post(HTTP_POST_URL, "y", 100) < 0) {};break;
      case 'z': while (http.post(HTTP_POST_URL, "z", 100) < 0) {};break;

      case 'A': while (http.post(HTTP_POST_URL, "A", 100) < 0) {};break;
      case 'B': while (http.post(HTTP_POST_URL, "B", 100) < 0) {};break;
      case 'C': while (http.post(HTTP_POST_URL, "C", 100) < 0) {};break;
      case 'D': while (http.post(HTTP_POST_URL, "D", 100) < 0) {};break;
      case 'E': while (http.post(HTTP_POST_URL, "E", 100) < 0) {};break;
      case 'F': while (http.post(HTTP_POST_URL, "F", 100) < 0) {};break;
      case 'G': while (http.post(HTTP_POST_URL, "G", 100) < 0) {};break;
      case 'H': while (http.post(HTTP_POST_URL, "H", 100) < 0) {};break;
      case 'I': while (http.post(HTTP_POST_URL, "I", 100) < 0) {};break;
      case 'J': while (http.post(HTTP_POST_URL, "J", 100) < 0) {};break;
      case 'K': while (http.post(HTTP_POST_URL, "K", 100) < 0) {};break;
      case 'L': while (http.post(HTTP_POST_URL, "L", 100) < 0) {};break;
      case 'M': while (http.post(HTTP_POST_URL, "M", 100) < 0) {};break;
      case 'N': while (http.post(HTTP_POST_URL, "N", 100) < 0) {};break;
      case 'O': while (http.post(HTTP_POST_URL, "O", 100) < 0) {};break;
      case 'P': while (http.post(HTTP_POST_URL, "P", 100) < 0) {};break;
      case 'Q': while (http.post(HTTP_POST_URL, "Q", 100) < 0) {};break;
      case 'R': while (http.post(HTTP_POST_URL, "R", 100) < 0) {};break;
      case 'S': while (http.post(HTTP_POST_URL, "S", 100) < 0) {};break;
      case 'T': while (http.post(HTTP_POST_URL, "T", 100) < 0) {};break;
      case 'U': while (http.post(HTTP_POST_URL, "U", 100) < 0) {};break;
      case 'V': while (http.post(HTTP_POST_URL, "V", 100) < 0) {};break;
      case 'W': while (http.post(HTTP_POST_URL, "W", 100) < 0) {};break;
      case 'X': while (http.post(HTTP_POST_URL, "X", 100) < 0) {};break;
      case 'Y': while (http.post(HTTP_POST_URL, "Y", 100) < 0) {};break;
      case 'Z': while (http.post(HTTP_POST_URL, "Z", 100) < 0) {};break;

      case '0': while (http.post(HTTP_POST_URL, "0", 100) < 0) {};break;
      case '2': while (http.post(HTTP_POST_URL, "2", 100) < 0) {};break;
      case '3': while (http.post(HTTP_POST_URL, "3", 100) < 0) {};break;
      case '4': while (http.post(HTTP_POST_URL, "4", 100) < 0) {};break;
      case '5': while (http.post(HTTP_POST_URL, "5", 100) < 0) {};break;
      case '6': while (http.post(HTTP_POST_URL, "6", 100) < 0) {};break;
      case '7': while (http.post(HTTP_POST_URL, "7", 100) < 0) {};break;
      case '8': while (http.post(HTTP_POST_URL, "8", 100) < 0) {};break;
      case '9': while (http.post(HTTP_POST_URL, "9", 100) < 0) {};break;

      case ']': while (http.post(HTTP_POST_URL, "''", 100) < 0) {};break;
      case '*': while (http.post(HTTP_POST_URL, "*", 100) < 0) {};break;
      case '%': while (http.post(HTTP_POST_URL, "%", 100) < 0) {};break;
      case '&': while (http.post(HTTP_POST_URL, "&", 100) < 0) {};break;
      case '(': while (http.post(HTTP_POST_URL, "(", 100) < 0) {};break;
      case ')': while (http.post(HTTP_POST_URL, ")", 100) < 0) {};break;
      case '_': while (http.post(HTTP_POST_URL, "_", 100) < 0) {};break;
      case '=': while (http.post(HTTP_POST_URL, "=", 100) < 0) {};break;
      case ',': while (http.post(HTTP_POST_URL, ",", 100) < 0) {};break;
      case '?': while (http.post(HTTP_POST_URL, "?", 100) < 0) {};break;
      case '<': while (http.post(HTTP_POST_URL, "é", 100) < 0) {};break;
      case '>': while (http.post(HTTP_POST_URL, "è", 100) < 0) {};break;
      case '/': while (http.post(HTTP_POST_URL, "/", 100) < 0) {};break;
      case '.': while (http.post(HTTP_POST_URL, ".", 100) < 0) {};break;
      case '{': while (http.post(HTTP_POST_URL, "1/", 100) < 0) {};break;
      case '}': while (http.post(HTTP_POST_URL, "ö", 100) < 0) {};break;
      case '+': while (http.post(HTTP_POST_URL, "+", 100) < 0) {};break;
      case '-': while (http.post(HTTP_POST_URL, "-", 100) < 0) {};break;
      case '^': while (http.post(HTTP_POST_URL, "^", 100) < 0) {};break;
      case '`': while (http.post(HTTP_POST_URL, "`", 100) < 0) {};break;

      case '$': while (http.post(HTTP_POST_URL, "§", 100) < 0) {};break;
      case '|': while (http.post(HTTP_POST_URL, "ü", 100) < 0) {};break;
      case '~': while (http.post(HTTP_POST_URL, "ä", 100) < 0) {};break;
      case ':': while (http.post(HTTP_POST_URL, ":", 100) < 0) {};break;
      case '#': while (http.post(HTTP_POST_URL, "#", 100) < 0) {};break;
      case '1': while (http.post(HTTP_POST_URL, "<br>", 100) < 0) {};
                while (http.post(HTTP_POST_URL, "designed by Andreas Eberhard", 100) < 0) {};
                while (http.post(HTTP_POST_URL, "<br>", 100) < 0) {};break;
      case '[': while (http.post(HTTP_POST_URL, "<br>", 100) < 0) {};break;
      case '!': while (http.post(HTTP_POST_URL, "!", 100) < 0) {};break;

      default: Serial.println("char not found");

      }

      while (wifly.receive((uint8_t *)&get, 1, 1000) == 1) {
      Serial.print(get);
      }

    
  }  
}
