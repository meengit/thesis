# Thesis

Master Thesis [Andreas Eberhard](https://www.masterstudiodesign.ch/en/students/andreas.eberhard)

University of Applied Sciences and Arts
Northwestern Switzerland FHNW

FHNW Academy of Art and Design

Institute Integrative Design
Masterstudio

## Structure

```bash
.
├── README.md     # This README.md
├── prototypes    # Keyboard prototypes
└── typewriter    # Arduino-based typewriter and webserver of exhibition object
```

## Credits

Thank you very much for your support [@signalwerk](https://github.com/signalwerk/keyboard-nine), Lukas Ruoff & Lukas Fischer

## License

[GNU AFFERO GENERAL PUBLIC LICENSE v3](./LICENSE)

